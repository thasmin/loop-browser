'use strict';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

// keep reference of window object so it's not garbage collected
let mainWindow;

function createWindow () {
	mainWindow = new BrowserWindow({width: 800, height: 600});
	mainWindow.loadURL('file://' + __dirname + '/index.html');
	//mainWindow.webContents.openDevTools();

	mainWindow.on('closed', function() {
		// dereference the window object
		mainWindow = null;
	});
}
app.on('ready', createWindow);

app.on('window-all-closed', function () {
	app.quit();
});

app.on('activate', function () {
	if (mainWindow === null) {
		createWindow();
	}
});
