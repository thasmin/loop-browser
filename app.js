var fs = require('fs');
var path = require('path');
var remote = require('electron').remote;

// f12 toggles dev tools, f5 refreshes
document.addEventListener("keydown", function (e) {
	if (e.which === 123) {
		remote.getCurrentWindow().toggleDevTools();
	} else if (e.which === 116) {
		location.reload();
	}
});

$(document).ready(function() {
	/*
	var api = remote.require('./api');
	$('#browser').html(api.test());
	*/

	$('#find').on('click', function() {
		$(':gt(2)', '#browser').remove();
		exploreDir();
	});
	exploreDir();

	$('#songsave').on('click', persistance.save);
	$('#songload').on('click', persistance.load);
});

function exploreDir(event) {
	var basedir = $('#basedir').val();
	if (arguments.length == 0) {
		var el = $('#browser');
	} else {
		event.stopPropagation();
		var el = $(this);
		if (el.hasClass('expanded')) {
			el.removeClass('expanded');
			$(':gt(0)', el).remove();
			return;
		}
		basedir = getPath(el);
	}

	fs.readdir(basedir, function(err, entries) {
		if (err) {
			el.html(err);
			return;
		}
		for (var i in entries) {
			var fullpath = path.join(basedir, entries[i]);
			if (fs.statSync(fullpath).isDirectory()) {
				var newdiv = $('<div>').addClass('dir').on('click', exploreDir);
				newdiv.append($('<div>').addClass('name').text(entries[i]));
				el.append(newdiv);
			} else if (fullpath.endsWith(".wav")) {
				el.append($('<div>').html(entries[i]).addClass('file').attr('draggable', 'true').on('click', playFile));
			}
		}
		el.addClass('expanded');
	});
}

function getPath(el) {
	var extra = el.text();
	for (var p = el.parent(); p.hasClass('dir'); p = p.parent())
		extra = path.join($(':first', p).text(), extra);
	return path.join($('#basedir').val(), extra);
}

function playFile(e) {
	event.stopPropagation();
	var fn = getPath($(this));
	music.play(fn);
}

var audioCtx = new window.AudioContext();
var buffers = {};

var music = {
	visibleMs: 2000,
	lastTime: 0,
	initialTime: 0,

	bpm: 140,
	beatMs: 1000 * 60 / 140 / 4,

	samples: {},

	playBuffer: function(buffer) {
		var src = audioCtx.createBufferSource();
		src.buffer = buffer;
		src.connect(audioCtx.destination);
		src.start();
	},
	play: function(fn) {
		if (fn in buffers) {
			music.playBuffer(buffers[fn]);
			return;
		}
		var req = new XMLHttpRequest();
		req.open("GET", fn, true);
		req.responseType = "arraybuffer";
		req.onload = function() {
			audioCtx.decodeAudioData(req.response, function(buffer) {
				music.playBuffer(buffer);
				buffers[fn] = buffer;
			});
		};
		req.send();
	},

	addSample: function(fn) {
		var sampleDiv = $('<div>')
			.addClass('sample')
			.width(music.beatMs * 16 / music.visibleMs * $('#samples').width())
			.data('file', fn)
			.text(path.basename(fn, '.wav'))
			//.on('click', music.arrangeSample)
			.appendTo('#samples');
		$('<div>')
			.addClass('x')
			.html('&times;')
			.on('click', music.removeSample)
			.appendTo(sampleDiv);

		var width = sampleDiv.width();
		for (var i = 0; i < 16; ++i) {
			var fudge = i == 0 ? 2.5 : i == 16-1 ? -2.5 : 0;
			$('<div>')
				.addClass('beat')
				.css('left', width * i / (16-1) - 2.5 + fudge)
				.on('click', music.toggleBeat)
				.appendTo(sampleDiv);
		}

		music.samples[fn] = [];
		return sampleDiv;
	},

	removeSample: function(e) {
		e.preventDefault();
		var $this = $(this);
		var fn = $this.parent().data('file');
		delete music.samples[fn];
		$this.parent().remove();
	},

	toggleBeat: function(e) {
		e.preventDefault();

		var $this = $(this);
		var childIdx = $this.index();
		var fn = $this.parent().data('file');
		var beatMs = childIdx * music.beatMs;
		var sampleIdx = music.samples[fn].indexOf(beatMs);

		$this.toggleClass('audible');
		if ($this.hasClass('audible')) {
			if (sampleIdx == -1)
				music.samples[fn].push(beatMs)
		} else {
			if (sampleIdx != -1)
				music.samples[fn].splice(sampleIdx, 1);
		}
	},
	/*
	arrangeSample: function() {
		var fn = $(this).data('file');
		var contents = $('<div class="arranger">').append($('#arranger').html());
		$('button.save', contents).on('click', function() {
			popup.hide();
			music.samples[fn] = [];
			var inputs = $('input', contents);
			for (var i = 0; i < inputs.length; ++i)
				if (inputs[i].checked)
					music.samples[fn].push(i * music.beatMs);
		});
		$('button.cancel', contents).on('click', popup.hide);
		popup.show(contents);
	},
	*/

	tick: function(time) {
		if (music.initialTime == 0)
			music.initialTime = time;

		var addl = time - music.lastTime;
		var musicWidth = $('#music').width();

		var play_start = (music.lastTime - music.initialTime) % music.visibleMs;
		var play_end = (time - music.initialTime) % music.visibleMs;
		for (var i in music.samples) {
			for (var j in music.samples[i]) {
				var sampletime = music.samples[i][j];
				if (play_start > play_end && (sampletime > play_start || sampletime < play_end))
					music.play(i);
				if (play_start < sampletime && sampletime < play_end)
					music.play(i);
			}
		}

		$('#arrow').css('left', play_end / music.visibleMs * musicWidth);

		music.lastTime = time;
		window.requestAnimationFrame(music.tick);
	}
};

var persistance = {
	save: function(e) {
		e.preventDefault();
		var songname = $('#songname').val();
		localStorage[songname] = JSON.stringify(music.samples);
	},

	load: function(e) {
		e.preventDefault();
		var songname = $('#songname').val();
		if (!songname in localStorage) {
			// TODO: error
			return;
		}

		music.samples = {};
		$('.sample').remove();

		var song = JSON.parse(localStorage[songname]);
		for (var fn in song) {
			var sampleDiv = music.addSample(fn);
			for (var i in song[fn]) {
				var childIdx = song[fn][i] / music.beatMs;
				sampleDiv.children('.beat').eq(childIdx - 1).addClass('audible');
			}
		}
		music.samples = song;
	}
};

var popup = {
	show: function(contents) {
		var $popup = $('#popup');
		$('#popup').show();
		$('#popup-bg').show();
		$popup.html('').append(contents)
			.css('margin-left', -$popup.width() / 2)
			.css('margin-top', -$popup.height() / 2);
	},
	hide: function() {
		$('#popup').hide();
		$('#popup-bg').hide();
	}
};

document.addEventListener('dragstart', function(e) {
});
$(document).on('dragstart', function(e) {
	e.originalEvent.dataTransfer.setData('text/plain', getPath($(e.target)));
});
$(document).on('ready', function(e) {
	$('#music')
		.on('dragover', function(e) { e.preventDefault(); })
		.on('drop', function(e) {
			var fn = e.originalEvent.dataTransfer.getData('text/plain');
			music.addSample(fn);
		});
	$('#popup-bg').on('click', popup.hide);
});

window.requestAnimationFrame(music.tick);
